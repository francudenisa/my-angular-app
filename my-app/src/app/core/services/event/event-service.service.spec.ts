import { EventServiceService } from '../event/event-service.service';
import { TestBed } from '@angular/core/testing';

describe('EventServiceService', () => {
  let service: EventServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
