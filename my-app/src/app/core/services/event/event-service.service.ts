import { Observable, Subject } from 'rxjs';

import { EventModel } from '../../../shared/models/event.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class EventService<T> {
  private readonly _events: EventModel<Subject<T>>[];

  constructor() {
    this._events = [];
  }

  broadcastEvent(key: string, value?: any) {
    const eventModel = this._events.find(event => {

      return event.key === key;
    });

    if (!eventModel) {
      this._events.push(new EventModel<Subject<T>>(key, new Subject<T>()));
    } else {
      eventModel.value.next(value);
    }
  }

  getEvent(key: string): Observable<T> {
    const eventModel = this._events.find(sub => {

      return sub.key === key;
    });

    if (eventModel) {

      return eventModel.value.asObservable();
    }
    {
      const subject = new Subject<T>();
      this._events.push(new EventModel<Subject<T>>(key, subject));

      return subject.asObservable();
    }
  }
}
