import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Teacher } from 'src/app/shared/models/teacher.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeachersRequestService {

  constructor(private readonly _httpClient: HttpClient) {}

  getTeachers(): Observable<Teacher[]>{

    return this._httpClient.get('./assets/json/teachers.min.json').pipe(
      map(result => result as Teacher[])
    );
  }

  getTeacherById(id: number): Observable<Teacher>{

    return this._httpClient.get('./assets/json/teachers.min.json').pipe(
      map(result => {
        const elementList = result as Teacher[];

        return elementList.find(m => m.nr === id);
      })
    );
  }

  deleteTeacherById(id: number): Observable<Teacher[]> {

    return this._httpClient.get('./assets/json/teachers.min.json').pipe(
      map(result => {
        let elementsList = result  as Teacher[];
        elementsList = elementsList.filter(m => m.nr !== id);

        return elementsList;
      })
    );
  }
}
