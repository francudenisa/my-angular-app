import { TestBed } from '@angular/core/testing';

import { TeachersRequestService } from './teachers-request.service';

describe('TeachersRequestService', () => {
  let service: TeachersRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TeachersRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
