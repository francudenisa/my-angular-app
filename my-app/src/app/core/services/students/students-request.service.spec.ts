import { TestBed } from '@angular/core/testing';

import { StudentsRequestService } from './students-request.service';

describe('StudentsRequestService', () => {
  let service: StudentsRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentsRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
