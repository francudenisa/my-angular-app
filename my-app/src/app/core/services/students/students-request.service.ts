import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from 'src/app/shared/models/student.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsRequestService {

  constructor(private readonly _httpClient: HttpClient) { }

  getStudents(): Observable<Student[]> {

    return this._httpClient.get('./assets/json/students.min.json').pipe(
      map(result => result as Student[])
    );
  }

  getStudentById(id: number): Observable<Student> {

    return this._httpClient.get('./assets/json/students.min.json').pipe(
      map(result => {
        const elementList = result as Student[];

        return elementList.find(m => m.nr === id);
      })
    );
  }

  deleteStudentById(id: number): Observable<Student[]> {

    return this._httpClient.get('./assets/json/students.min.json').pipe(
      map(result => {
        let elementsList = result  as Student[];
        elementsList = elementsList.filter(m => m.nr !== id);

        return elementsList;
      })
    );
  }
}
