import { Course } from 'src/app/shared/models/course.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CoursesRequestService {

  constructor(private readonly _httpClient: HttpClient) { }

  getCourses(): Observable<Course[]> {

    return this._httpClient.get('./assets/json/courses.min.json').pipe(
      map(result => result as Course[])
    );
  }

  getCourseById(id: number): Observable<Course>{

    return this._httpClient.get('./assets/json/courses.min.json').pipe(
      map(result => {
        const elementList = result as Course[];

        return elementList.find(m => m.nr === id);
      })
    );
  }

  deleteCourseById(id: number): Observable<Course[]> {

    return this._httpClient.get('./assets/json/courses.min.json').pipe(
      map(result => {
        let elementsList = result  as Course[];
        elementsList = elementsList.filter(m => m.nr !== id);

        return elementsList;
      })
    );
  }
}
