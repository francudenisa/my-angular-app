import { TestBed } from '@angular/core/testing';

import { CoursesRequestService } from './courses-request.service';

describe('CoursesRequestService', () => {
  let service: CoursesRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoursesRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
