import { CommonModule } from '@angular/common';
import { EmptyLayoutComponent } from './components/empty-layout/empty-layout.component';
import { HeaderComponent } from './components/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material/material.module';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { SidenavService } from './services/sidenav/sidenav.service';
import { StudentsRequestService } from './services/students/students-request.service';

@NgModule({
  declarations: [
    HeaderComponent,
    SidenavComponent,
    EmptyLayoutComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    HttpClientModule
  ],
  exports: [
    HeaderComponent,
    SidenavComponent
  ],
  providers: [
    SidenavService,
    StudentsRequestService
  ]
})
export class CoreModule { }
