import { Component, Injectable, OnInit, Output } from '@angular/core';

import { SidenavService } from '../../services/sidenav/sidenav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

@Injectable ({
  providedIn: 'root'
})

export class HeaderComponent implements OnInit {

  messageFromButton: boolean;
  router: any;
  constructor(private readonly _sidenavService: SidenavService) { }

  ngOnInit(): void {
  }

  toggleSidenav(){
    this.router.navigateByUrl('');
  }
}
