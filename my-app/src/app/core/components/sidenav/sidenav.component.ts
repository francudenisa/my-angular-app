import { ChangeDetectorRef, Component, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  constructor(private readonly _detectChanges: ChangeDetectorRef) {
   }

  ngOnInit(): void {
  }
}
