import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { Button } from '../../../shared/interfaces/button';
import { DialogButtonFactory } from '../dialog-button-factory/dialog-button.factory';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class ButtonFactory {
  public button = [];

  constructor(private readonly _router: Router,
    public dialog: MatDialog) { }

  public createSearchButton(): Button {

    return{
      icon: 'search',
      tooltip: 'Search',
      method: () => {}
    };
  }

  public createAddButton(): Button {

    return{
      icon: 'add',
      tooltip: 'Add',
      method: () => {}
    };
  }

  public createSortButton(): Button {

    return{
      icon: 'swap_vert',
      tooltip: 'Sort',
      method: () => {}
    };
  }

  public createTileViewButton(): Button {

    return{
      icon: 'solid_view_tile',
      tooltip: 'Tile view',
      method: () => {}
    };
  }

  public createListViewButton(): Button {

    return{
      icon: 'solid_view_list',
      tooltip: 'List view',
      method: () => {}
    };
  }

  public createBackButton(redirectUrl: string): Button {

    return{
      icon: 'arrow_back_outline',
      tooltip: 'Back',
      method: () => {
        this._router.navigate([redirectUrl]);
      }
    };
  }

  public createDeleteButton(): Button {

    return{
      icon: 'trash_outline',
      tooltip: 'Delete',
      method: () => {}
    };
  }

  public createEditButton(): Button {

    return{
      icon: 'edit',
      tooltip: 'Edit',
      method: () => {}
    };
  }
}
