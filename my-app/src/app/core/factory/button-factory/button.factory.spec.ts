import { ButtonFactory } from './button.factory';
import { TestBed } from '@angular/core/testing';

describe('ButtonFactoryService', () => {
  let service: ButtonFactory;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ButtonFactory);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
