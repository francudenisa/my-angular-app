import { DialogButton } from 'src/app/shared/interfaces/dialog-button';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class DialogButtonFactory {

  constructor() { }

  public createYesButton(): DialogButton {

    return{
      name: 'Yes',
      method: () => {}
    };
  }

  public createNoButton(): DialogButton {

    return{
      name: 'No',
      method: () => {}
    };
  }
}
