import { DialogButtonFactory } from './dialog-button.factory';
import { TestBed } from '@angular/core/testing';

describe('DialogButtonFactory', () => {
  let service: DialogButtonFactory;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DialogButtonFactory);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
