import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Button } from 'src/app/shared/interfaces/button';
import { ButtonFactory } from 'src/app/core/factory/button-factory/button.factory';
import { Course } from 'src/app/shared/models/course.model';
import { CoursesRequestService } from 'src/app/core/services/courses/courses-request.service';
import { DialogButtonFactory } from 'src/app/core/factory/dialog-button-factory/dialog-button.factory';
import { DialogComponent } from 'src/app/shared/components/dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {
  public title: string;
  public toolbarButtons = [];
  public element: Course = new Course();
  public backButton: Button;

  private _courseId: string;
  private _id: number;

  constructor(private readonly _route: ActivatedRoute,
              private readonly _coursesRequestService: CoursesRequestService,
              private readonly _buttonFactory: ButtonFactory,
              private readonly _dialogButtonFactory: DialogButtonFactory,
              public dialog: MatDialog,
              private readonly _router: Router) { }


  ngOnInit(): void {
    this._courseId = this._route.snapshot.paramMap.get('courseId');
    this.title = 'Course Details';
    this._id = Number(this._courseId);
    this._coursesRequestService.getCourseById(this._id).subscribe
      (res => {
        this.element = res;
      }
      );
    this.backButton = this._buttonFactory.createBackButton('/courses');
    const deleteButton = this._buttonFactory.createDeleteButton();
    deleteButton.method = () => this.openDialog();
    this.toolbarButtons.push(deleteButton);
    this.toolbarButtons.push(this._buttonFactory.createEditButton());
  }

  openDialog(): void {
    console.log('open dialog from details');
    let dialogRef;
    const yesButton = this._dialogButtonFactory.createYesButton();
    yesButton.method = () => {
      this._coursesRequestService.deleteCourseById(this._id).subscribe(() => {
          dialogRef.close();
          this._router.navigate(['/courses']);
         }
     );
    };
    const noButton = this._dialogButtonFactory.createNoButton();
    dialogRef = this.dialog.open(
      DialogComponent, {
        height: '200px',
        width: '300px',
        data: {
          name: 'Confirm',
          content: 'Are you sure you want to delete this element?',
          submitButton: yesButton,
          cancelButton: noButton
        }
      }
    );
    dialogRef.afterClosed();
  }

}
