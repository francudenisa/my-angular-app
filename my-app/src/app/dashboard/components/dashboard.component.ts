import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  startIndex: number;
  public imagesUrl: string[];

  constructor() { }

  ngOnInit(): void {
    this.imagesUrl = ['/assets/pictures/sigla-unitbv.png', '/assets/pictures/sigla-unitbv-mi.jpg', '/assets/pictures/background.jpg'];
  }
}
