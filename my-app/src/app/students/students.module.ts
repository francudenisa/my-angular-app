import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { StudentsListComponent } from './components/students-list/students-list.component';
import { StudentsRoutingModule } from './students-routing.module';
import { StudentDetailsComponent } from './components/student-details/student-details.component';

@NgModule({
  declarations: [StudentsListComponent, StudentDetailsComponent],
  imports: [
    CommonModule,
    StudentsRoutingModule,
    SharedModule
  ]
})
export class StudentsModule { }
