import { BehaviorSubject, Subject } from 'rxjs';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Button } from 'src/app/shared/interfaces/button';
import { ButtonFactory } from 'src/app/core/factory/button-factory/button.factory';
import { Column } from 'src/app/shared/models/column.model';
import { DialogButtonFactory } from 'src/app/core/factory/dialog-button-factory/dialog-button.factory';
import { DialogComponent } from 'src/app/shared/components/dialog/dialog.component';
import { EventService } from 'src/app/core/services/event/event-service.service';
import { EventServiceKey } from 'src/app/shared/enums/event-service-key.enum';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Student } from 'src/app/shared/models/student.model';
import { StudentView } from 'src/app/shared/view-models/student-view.model';
import { StudentsRequestService } from 'src/app/core/services/students/students-request.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})
export class StudentsListComponent implements OnInit, OnDestroy {

  public displayedColumns: Column[] = [
    { name: 'nr', label: 'Nr', style: 'width-5'},
    { name: 'name', label: 'Name', style: 'width-15' },
    { name: 'surname', label: 'Surname', style: 'width-15' },
    { name: 'mail', label: 'Mail', style: 'width-20' },
    { name: 'phone', label: 'Phone', style: 'width-10' },
    { name: 'specialization', label: 'Specialization', style: 'width-15' },
    { name: 'group', label: 'Group', style: 'width-10' }
  ];
  public data;
  public dataSource: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  public length: Subject<number> = new Subject();
  public title: string;
  public toolbarButtons = [];
  public redirectUrl: string;
  public backButton: Button;
  public actionsButtons: Button[] = [];

  private _pageIndex: number = 0;
  private _pageSize: number = 10;
  private _elementsList: any[] = [];
  private _length: number = 0;
  private readonly _onDestroy: Subject<void> = new Subject<void>();

  constructor(private readonly _buttonFactory: ButtonFactory,
              private readonly _studentsRequestService: StudentsRequestService,
              private readonly _dialogButtonFactory: DialogButtonFactory,
              public dialog: MatDialog,
              private readonly _eventService: EventService<any>) {

                this._eventService.getEvent(EventServiceKey.DeleteStudent).pipe(takeUntil(this._onDestroy)).subscribe(
                  param => this.openDialog(param)
                );
              }
  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  ngOnInit(): void {
    this.getStudentsList();
    this.redirectUrl = '/students/{0}';
    this.initializeToolbar();
  }

  openDialog(id: number): void {
    let dialogRef;
    console.log('open dialog from list');
    const yesButton = this._dialogButtonFactory.createYesButton();
    yesButton.method = () => {
      this._studentsRequestService.deleteStudentById(id).subscribe(response => {
        dialogRef.close();
        this.setDataSource(response);
        }
      );
    };
    const noButton = this._dialogButtonFactory.createNoButton();
    dialogRef = this.dialog.open(
      DialogComponent, {
        height: '200px',
        width: '300px',
        data: {
          name: 'Confirm',
          content: 'Are you sure you want to delete this element?',
          submitButton: yesButton,
          cancelButton: noButton
        }
      }
    );
    dialogRef.afterClosed();
  }

  setDataSourceWithPagination(event: PageEvent) {
    this._pageIndex = event.pageIndex;
    this._pageSize = event.pageSize;
    this.length.next(this._elementsList.length);
    this.getPaginatedData();
  }

  private initializeToolbar() {
    this.toolbarButtons.push(this._buttonFactory.createSearchButton());
    this.toolbarButtons.push(this._buttonFactory.createSortButton());
    this.toolbarButtons.push(this._buttonFactory.createListViewButton());
  }

  private getStudentsList() {
    this._studentsRequestService.getStudents().subscribe
      (res => {
        this.setDataSource(res);
      }
    );
  }

  private setDataSource(items: Student[]) {
    this._elementsList = items.map(item => new StudentView(item, this._buttonFactory, this._eventService));
    this.getPaginatedData();
    this.length.next(this._elementsList.length);
    this._length = this._elementsList.length;
    this.title = `Students (${this._length})`;
  }

  private getPaginatedData() {
    let elements: any[] = [];

    for (let i = this._pageIndex * this._pageSize; i < (this._pageIndex + 1) * this._pageSize; i++) {

      if (this._elementsList[i]) {
        elements.push(this._elementsList[i]);
      }
    }
    this.dataSource.next(elements);
  }
}
