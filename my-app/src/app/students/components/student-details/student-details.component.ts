import { ActivatedRoute, Router } from '@angular/router';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { Button } from 'src/app/shared/interfaces/button';
import { ButtonFactory } from 'src/app/core/factory/button-factory/button.factory';
import { DialogButtonFactory } from 'src/app/core/factory/dialog-button-factory/dialog-button.factory';
import { DialogComponent } from 'src/app/shared/components/dialog/dialog.component';
import { EventService } from 'src/app/core/services/event/event-service.service';
import { EventServiceKey } from 'src/app/shared/enums/event-service-key.enum';
import { MatDialog } from '@angular/material/dialog';
import { Student } from 'src/app/shared/models/student.model';
import { StudentsRequestService } from 'src/app/core/services/students/students-request.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  public title: string;
  public toolbarButtons: Button[] = [];
  public element: Student = new Student();
  public backButton: Button;

  private _studentId: string;
  private _id: number;

  constructor(private readonly _route: ActivatedRoute,
              private readonly _studentsRequestService: StudentsRequestService,
              private readonly _buttonFactory: ButtonFactory,
              private readonly _dialogButtonFactory: DialogButtonFactory,
              public dialog: MatDialog,
              private readonly _router: Router) {
              }

  ngOnInit(): void {
    this._studentId = this._route.snapshot.paramMap.get('studentId');
    this.title = 'Student Details';
    this._id = Number(this._studentId);
    this._studentsRequestService.getStudentById(this._id).subscribe
      (res => {
        this.element = res;
      }
      );
    this.backButton = this._buttonFactory.createBackButton('/students');
    const deleteButton = this._buttonFactory.createDeleteButton();
    deleteButton.method = () => this.openDialog();
    this.toolbarButtons.push(deleteButton);
    this.toolbarButtons.push(this._buttonFactory.createEditButton());
  }

  openDialog(): void {
    console.log('open dialog from details');
    let dialogRef;
    const yesButton = this._dialogButtonFactory.createYesButton();
    yesButton.method = () => {
      this._studentsRequestService.deleteStudentById(this._id).subscribe(() => {
          dialogRef.close();
          this._router.navigate(['/students']);
         }
     );
    };
    const noButton = this._dialogButtonFactory.createNoButton();
    dialogRef = this.dialog.open(
      DialogComponent, {
        height: '200px',
        width: '300px',
        data: {
          name: 'Confirm',
          content: 'Are you sure you want to delete this element?',
          submitButton: yesButton,
          cancelButton: noButton
        }
      }
    );
    dialogRef.afterClosed();
  }
}
