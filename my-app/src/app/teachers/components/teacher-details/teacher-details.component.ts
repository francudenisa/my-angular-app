import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Button } from 'src/app/shared/interfaces/button';
import { ButtonFactory } from 'src/app/core/factory/button-factory/button.factory';
import { DialogButtonFactory } from 'src/app/core/factory/dialog-button-factory/dialog-button.factory';
import { DialogComponent } from 'src/app/shared/components/dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Teacher } from 'src/app/shared/models/teacher.model';
import { TeachersRequestService } from 'src/app/core/services/teachers/teachers-request.service';

@Component({
  selector: 'app-teacher-details',
  templateUrl: './teacher-details.component.html',
  styleUrls: ['./teacher-details.component.css']
})
export class TeacherDetailsComponent implements OnInit {
  public title: string;
  public toolbarButtons = [];
  public element: Teacher = new Teacher();
  public backButton: Button;

  private _teacherId: string;
  private _id: number;

  constructor(private readonly _route: ActivatedRoute,
              private readonly _teachersRequestService: TeachersRequestService,
              private readonly _buttonFactory: ButtonFactory,
              private readonly _dialogButtonFactory: DialogButtonFactory,
              public dialog: MatDialog,
              private readonly _router: Router) { }


  ngOnInit(): void {
    this._teacherId = this._route.snapshot.paramMap.get('teacherId');
    this.title = 'Teacher Details';
    this._id = Number(this._teacherId);
    this._teachersRequestService.getTeacherById(this._id).subscribe(
      res => {
      this.element = res;
    }
    );
    this.backButton = this._buttonFactory.createBackButton('/teachers');
    const deleteButton = this._buttonFactory.createDeleteButton();
    deleteButton.method = () => this.openDialog();
    this.toolbarButtons.push(deleteButton);
    this.toolbarButtons.push(this._buttonFactory.createEditButton());
    }

    openDialog(): void {
      console.log('open dialog from details');
      let dialogRef;
      const yesButton = this._dialogButtonFactory.createYesButton();
      yesButton.method = () => {
        this._teachersRequestService.deleteTeacherById(this._id).subscribe(() => {
            dialogRef.close();
            this._router.navigate(['/teachers']);
           }
       );
      };
      const noButton = this._dialogButtonFactory.createNoButton();
      dialogRef = this.dialog.open(
        DialogComponent, {
          height: '200px',
          width: '300px',
          data: {
            name: 'Confirm',
            content: 'Are you sure you want to delete this element?',
            submitButton: yesButton,
            cancelButton: noButton
          }
        }
      );
      dialogRef.afterClosed();
    }
}
