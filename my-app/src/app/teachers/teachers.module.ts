import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TeachersListComponent } from './components/teachers-list/teachers-list.component';
import { TeachersRoutingModule } from './teachers-routing.module';
import { TeacherDetailsComponent } from './components/teacher-details/teacher-details.component';

@NgModule({
  declarations: [TeachersListComponent, TeacherDetailsComponent],
  imports: [
    CommonModule,
    TeachersRoutingModule,
    SharedModule
  ]
})
export class TeachersModule { }
