import { Component, OnInit, ViewChild } from '@angular/core';

import { MatSidenav } from '@angular/material/sidenav';
import { SidenavService } from './core/services/sidenav/sidenav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('snav') public sidenav: MatSidenav;

  constructor(private readonly _sidenavService: SidenavService) {
   
  }

  ngOnInit(): void {
    this._sidenavService.setSidenav(this.sidenav);
  }

  title = 'my-app';

}
