import { Component, Input, OnInit } from '@angular/core';

import { Button } from 'src/app/shared/interfaces/button';
import {Location} from '@angular/common';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  @Input() title: string;
  @Input() toolbarButtons: Button;
  @Input() backButton: Button;

  constructor(private readonly _location: Location) { }

  isTileView: boolean = false;
  isListView: boolean = true;

  ngOnInit(): void {
  }

  onBackClick() {
    this._location.back();
  }
}
