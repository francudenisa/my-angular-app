import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

import { Subject } from 'rxjs';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

  @Input() length: Subject<number> = new Subject();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Output() pageEvent = new EventEmitter<PageEvent>();

  pageIndex: number;
  pageSize: number = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.length.subscribe((value) => {

      if (value >= 0) {
        this.paginator.length = value;
      }
    });
  }

  onPageChanged(e) {
    this.pageIndex = e.pageIndex;
    this.pageSize = e.pageSize;
    this.pageEvent.emit(e);
  }
}
