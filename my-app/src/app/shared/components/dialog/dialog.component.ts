import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../interfaces/dialog-data';
import { DialogButton } from '../../interfaces/dialog-button';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})

export class DialogComponent implements OnInit {

  submitButton: DialogButton;
  cancelButton: DialogButton;

  constructor(public dialogRef: MatDialogRef<MatDialog>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
                this.submitButton = data.submitButton;
                this.cancelButton = data.cancelButton;
              }

  ngOnInit(): void {
  }

  onClickNo() {
    this.dialogRef.close();
  }

}
