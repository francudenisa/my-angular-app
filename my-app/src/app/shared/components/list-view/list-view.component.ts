import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

import { Button } from '../../interfaces/button';
import { Column } from '../../models/column.model';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { String } from 'typescript-string-operations';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {
  @Input() dataSource: MatTableDataSource<any>;
  @Input() displayedColumns: Column[];
  @Input() redirectUrl: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public columns: string[] = [];
  public isElementClicked: boolean = false;
  public button: Button;

  constructor(private readonly _router: Router,
              public dialog: MatDialog) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.columns = this.displayedColumns.map(m => m.name);
    this.columns.push('actionsColumn');
  }

  getValue(element, column) {

    return this.columns[element];
  }

  redirectToUrl(id: number) {
    this._router.navigate([String.Format(this.redirectUrl, id)]);
  }
}
