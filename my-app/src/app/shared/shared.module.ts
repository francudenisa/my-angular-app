import { CommonModule } from '@angular/common';
import { DialogComponent } from './components/dialog/dialog.component';
import { ListViewComponent } from './components/list-view/list-view.component';
import {MatTableModule} from '@angular/material/table';
import { MaterialModule } from '../material/material.module';
import { NgModule } from '@angular/core';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';

@NgModule({
  declarations: [ListViewComponent, PaginatorComponent, ToolbarComponent, DialogComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MaterialModule
  ],
  exports: [
    ListViewComponent,
    PaginatorComponent,
    ToolbarComponent,
    DialogComponent
  ]
})
export class SharedModule { }
