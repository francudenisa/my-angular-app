export interface DialogButton {
  name: string;
  method(): void;
}
