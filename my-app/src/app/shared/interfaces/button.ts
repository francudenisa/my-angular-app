export interface Button {
    icon: string;
    tooltip: string;
    method(): void;
  }
