import { DialogButton } from './dialog-button';

export interface DialogData {
  name: string;
  content: string;
  submitButton: DialogButton;
  cancelButton: DialogButton;
}
