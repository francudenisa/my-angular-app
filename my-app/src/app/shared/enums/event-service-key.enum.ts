export enum EventServiceKey {
  DeleteStudent = 'deleteStudent',
  DeleteTeacher = 'deleteTeacher',
  DeleteCourse = 'deleteCourse'
}
