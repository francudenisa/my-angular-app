export class Student {
    public nr: number;
    public name: string;
    public surname: string;
    public mail: string;
    public phone: string;
    public specialization: string;
    public group: string;
  }
