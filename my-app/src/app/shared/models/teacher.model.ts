export class Teacher {
    public nr: number;
    public name: string;
    public surname: string;
    public mail: string;
    public phone: string;
  }
