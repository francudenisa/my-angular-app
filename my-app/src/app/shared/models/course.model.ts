export class Course {
    public nr: number;
    public name: string;
    public year: string;
    public semester: string;
    public course: string;
    public laboratory: string;
    public lecture: string;
    public credits: string;
  }
