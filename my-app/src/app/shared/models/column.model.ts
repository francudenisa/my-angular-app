export class Column {
  public name: string;
  public label: string;
  public style: string;
}
