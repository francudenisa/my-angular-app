import { Button } from '../interfaces/button';
import { ButtonFactory } from 'src/app/core/factory/button-factory/button.factory';
import { Course } from '../models/course.model';
import { EventService } from 'src/app/core/services/event/event-service.service';
import { EventServiceKey } from '../enums/event-service-key.enum';

export class CourseView extends Course {
  public actionsButtons: Button[] = [];

  constructor(public item: Course,
              private readonly _buttonFactory: ButtonFactory,
              private readonly _eventService: EventService<any>) {
    super();
    Object.assign(this, item);
    this.InitializeButtons();
  }

  private InitializeButtons() {
    const deleteButton = this._buttonFactory.createDeleteButton();
    deleteButton.method = () => this._eventService.broadcastEvent(EventServiceKey.DeleteCourse, this.item.nr);
    this.actionsButtons.push(deleteButton);
    this.actionsButtons.push(this._buttonFactory.createEditButton());
  }
}
