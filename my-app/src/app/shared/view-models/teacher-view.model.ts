import { Button } from '../interfaces/button';
import { ButtonFactory } from 'src/app/core/factory/button-factory/button.factory';
import { EventService } from 'src/app/core/services/event/event-service.service';
import { EventServiceKey } from '../enums/event-service-key.enum';
import { Teacher } from '../models/teacher.model';

export class TeacherView extends Teacher {
  public actionsButtons: Button[] = [];

  constructor(public item: Teacher,
              private readonly _buttonFactory: ButtonFactory,
              private readonly _eventService: EventService<any>) {
    super();
    Object.assign(this, item);
    this.InitializeButtons();
  }

  private InitializeButtons() {
    const deleteButton = this._buttonFactory.createDeleteButton();
    deleteButton.method = () => this._eventService.broadcastEvent(EventServiceKey.DeleteTeacher, this.item.nr);
    this.actionsButtons.push(deleteButton);
    this.actionsButtons.push(this._buttonFactory.createEditButton());
  }
}
