import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from './core/core.module';
import { MaterialModule } from './material/material.module';
import { NgModule } from '@angular/core';
import { StudentsModule } from './students/students.module';
import {MatProgressBarModule} from '@angular/material/progress-bar';
// const appRoutes: Routes = [
//   { path: '', component: StudentsListComponent, data: { title: 'Students List Component' } },
//   { path: 'first', component: StudentsListComponent, data: { title: 'Students List Component' } },
//   { path: 'second', component: CoursesListComponent, data: { title: 'Courses List Component' } }
// ];



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    CoreModule,
    StudentsModule,
    MatProgressBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
